
package obligatorioalgoritmos1;


public class ListaAmbulancias implements IListaAmbulancias {
  public NodoAmbulancia amb;
  public NodoAmbulancia ambult;
  public int elementos;
  public ABBCiudades ciudades;
  
   public NodoAmbulancia getambult() {
        return ambult;
    }
    public void setAmbult(NodoAmbulancia ambult) {
        this.ambult = ambult;
    }

    
    public NodoAmbulancia getAmb() {
        return amb;
    }

    public void setAmb(NodoAmbulancia amb) {
        this.amb = amb;
    }

    public int getElementos() {
        return elementos;
    }

    public void setElementos(int elementos) {
        this.elementos = elementos;
    }

    public ABBCiudades getCiudades() {
        return ciudades;
    }

    public void setCiudades(ABBCiudades ciudades) {
        this.ciudades = ciudades;
    }

    public ListaAmbulancias() {
        this.amb = null;
        this.ambult=null;
        this.elementos = 0;
        this.ciudades = null;
    }
  
    @Override
    public void agregar(String o) {
         
            
    }

    @Override
    public void eliminar(Object o) {
        NodoAmbulancia nuevo=(NodoAmbulancia)o;
		if(amb.getAmbulanciaID().equals(nuevo.getAmbulanciaID())){
                    amb.getChoferes().eliminar(o);
                    amb = amb.getSiguiente();
			elementos--;
		}
		else{
			NodoAmbulancia aux = amb;
			while(aux.getSiguiente()!=null)
			{
				if(aux.getSiguiente().getAmbulanciaID().equals(nuevo.getAmbulanciaID()))
				{
					aux.setSiguiente(aux.getSiguiente().getSiguiente());
					elementos--;
					
				}
				else
					aux = aux.getSiguiente();
			}
		}
	
    }

    @Override
    public boolean estaVacia() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean estaLlena() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean existe(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int cantidadNodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     @Override
    public NodoAmbulancia buscarAmbulancia(String AmbulanciaID){
    
        NodoAmbulancia aux = this.getAmb();
           while (aux !=null && aux.getAmbulanciaID()!= AmbulanciaID){
                  aux=aux.getSiguiente();               
           }
           return aux;
        
    }

    
    
    
    
     
    
    
}
