
package obligatorioalgoritmos1;


public class ObligatorioAlgoritmos1 {

    
    public static void main(String[] args) {
        
        Prueba p = new Prueba();
        Sistema s = new Sistema();
        
        prueba1(p, s);
        
        
        
        
        
    }
    
   static  public void prueba1 (Prueba p , Sistema s )
    {
    p.ver(s.crearSistemaDeEmergencias(5), Isistema.TipoRet.OK, "El Sistema esta Implementado ");
    p.ver(s.crearSistemaDeEmergencias(-25), Isistema.TipoRet.ERROR, "El Numero de Ciudades es negativo ");
    
    //p.ver(s.destruirSistemaEmergencias(), Isistema.TipoRet.OK, " El Sistema se ah vaciado correctamente  ");
    
    p.ver(s.registrarAmbulancia("AMB_MV1",2), Isistema.TipoRet.OK, "Se agrega correctamente ambulancia  ");
    p.ver(s.registrarAmbulancia("AMB_TL3",4), Isistema.TipoRet.OK, "Se agrega correctamente ambulancia  ");
    p.ver(s.registrarAmbulancia("AMB_MV3",9999), Isistema.TipoRet.ERROR, "La ciudad 999 no se puede encontrar   ");
    p.ver(s.registrarAmbulancia("AMB_MV2",3), Isistema.TipoRet.ERROR, "Ya existe una ambulancia con identificador AMBULANCIAID ");
    
    p.ver(s.deshabilitarAmbulancia("AMB_MV1"), Isistema.TipoRet.OK, "Se deshabilita la ambulancia ");
    p.ver(s.deshabilitarAmbulancia("AMB_MV999"), Isistema.TipoRet.ERROR, "No existe una ambulancia con identificador AmbulanciaID ");
    p.ver(s.deshabilitarAmbulancia("AMB_MV1"), Isistema.TipoRet.ERROR, "La ambulancia AmbulanciaID ya esta en estado NO_Disponible  ");
    p.ver(s.deshabilitarAmbulancia("AMB_MV3"), Isistema.TipoRet.ERROR, "No es posible deshabilitar la ambulanciaID porque esta en viaje ");
        
    p.ver(s.habilitarAmbulancia("AMB_MV1"), Isistema.TipoRet.OK, "La ambulancia ambulanciaID se habilita ");
    p.ver(s.habilitarAmbulancia("AMB_MV888"), Isistema.TipoRet.ERROR, "No existe una ambulancia con identificador AmbulanciaID ");
    p.ver(s.habilitarAmbulancia("AMB_MV10"), Isistema.TipoRet.ERROR, "La ambulancia AmbulanciaID ya esta en estado Disponible  ");
    
     p.ver(s.eliminarAmbulancia("AMB_MV11"), Isistema.TipoRet.OK, "Se Elimina la ambulancia AMBULANCIAID  ");
     p.ver(s.eliminarAmbulancia("AMB_MV2222"), Isistema.TipoRet.ERROR, "No existe una ambulancia con identificador AmbulanciaID  ");
     p.ver(s.eliminarAmbulancia("AMB_MV11"), Isistema.TipoRet.ERROR, "No es posible eliminar la ambulancia AMBULANCIAID  ");
     
    ////////////////// VER DETALLES CON PROFED DE LA LETRA //////
     p.ver(s.buscarAmbulancia("AMB_TL3"), Isistema.TipoRet.OK, "Se encuentra ambulancia ambulanciaID ");
     p.ver(s.buscarAmbulancia("AMB_TL33333"), Isistema.TipoRet.ERROR, "No existe una  ambulancia ambulanciaID ");
      ////////////////////////////////////////////////////////////////
      
      ////////////////// VER DETALLES CON EL PROFE //////////////
     p.ver(s.informeAmbulancia(), Isistema.TipoRet.OK, "No existe una  ambulancia ambulanciaID ");
     
      ///////////////////////////////
      //////////// este falta  diseño //////////
     p.ver(s.informeAmbulancia(3), Isistema.TipoRet.OK, " ");
     p.ver(s.informeAmbulancia(23), Isistema.TipoRet.ERROR, " La Ciudad ciudadID no existe  ");
      
     
     p.ver(s.cambiarUbicacion("AMB_MV2", 3), Isistema.TipoRet.OK, "Se cambia Ubicacion de la ambulancia  ");
     p.ver(s.cambiarUbicacion("AMB_MV2", 99999), Isistema.TipoRet.ERROR, "La ciudad ciudadId no existe   ");
     p.ver(s.cambiarUbicacion("AMB_MV23232", 99999), Isistema.TipoRet.ERROR, " La ambulancia no existe en el sistema ");
     
     p.ver(s.agregarCiudad("MONTEVIDEO"), Isistema.TipoRet.OK, "Se crea una nueva ciudad  ");
     p.ver(s.agregarCiudad("SALTO"), Isistema.TipoRet.OK, "Se crea una nueva ciudad  ");
     p.ver(s.agregarCiudad("TACUAREMBO"), Isistema.TipoRet.OK, "Se crea una nueva ciudad  ");
     p.ver(s.agregarCiudad("SAN JOSE"), Isistema.TipoRet.OK, "Se crea una nueva ciudad  ");
     p.ver(s.agregarCiudad("RIVERA "), Isistema.TipoRet.OK, "Se crea una nueva ciudad  ");
     p.ver(s.agregarCiudad("DURAZNO"), Isistema.TipoRet.ERROR, " No se pueden ingresar la ciudadNombre en el sistema por no tener mas capacidad ");
     
     
     //// FALTA ver la formato
      p.ver(s.listarCiudades(), Isistema.TipoRet.OK, "  ");
      
      
      p.ver(s.agregarRuta(1, 2, 23), Isistema.TipoRet.OK, " Se agrega sastifactoriamente la ruta   ");
      p.ver(s.agregarRuta(1111, 2, 23), Isistema.TipoRet.ERROR, " La ciudad ciudadORigen no exsite   ");
      p.ver(s.agregarRuta(2, 29999, 25), Isistema.TipoRet.ERROR, " La ciudad ciudadDestino no exsite   ");
      p.ver(s.agregarRuta(2, 29999,-25), Isistema.TipoRet.ERROR, " La duracion del viaje debe ser mayor que 0 ");
      
      
       p.ver(s.modificarDemora(1, 2, 23), Isistema.TipoRet.OK, " Se cambia la demora sastifactoriamente ");
       p.ver(s.modificarDemora(111, 2, 23), Isistema.TipoRet.ERROR, "La ciudad ciudadORigen no exsite  ");
       p.ver(s.modificarDemora(2, 29999, 25), Isistema.TipoRet.ERROR, " La ciudad ciudadDestino no exsite   ");
       p.ver(s.agregarRuta(2, 29999,-25), Isistema.TipoRet.ERROR, " La duracion del viaje debe ser mayor que 0 ");
       
       
       ///////////////////////////////////////// formato 
       p.ver(s.ambulanciaMasCercana(2), Isistema.TipoRet.OK, " Ambulancia mas cercana a  ");
       p.ver(s.ambulanciaMasCercana(222232), Isistema.TipoRet.ERROR, " La ciudad ciudadID no existe      ");
       
       ///////////// formato
       p.ver(s.rutaMasRapida(2,3), Isistema.TipoRet.OK, "  ");
       p.ver(s.rutaMasRapida(22223,3), Isistema.TipoRet.ERROR, " CiudadOrigen no existe   ");
       p.ver(s.rutaMasRapida(2,223232322), Isistema.TipoRet.ERROR, "La CiudadDestino no existe  ");
       
       
       ////////formato 
       p.ver(s.informeCiudades(), Isistema.TipoRet.OK, " ");
       
       //formato
       p.ver(s.ciudadesEnRadio(2,25), Isistema.TipoRet.OK, "  ");
       p.ver(s.ciudadesEnRadio(222222,25), Isistema.TipoRet.ERROR, " LA ciudad Introducida no existe   ");
       p.ver(s.ciudadesEnRadio(2,-25), Isistema.TipoRet.ERROR, " La duracion del viaje debe ser mayor a 0    ");
       
       
       p.ver(s.registrarChofer("AMBMVD1","JORGE","46559714"), Isistema.TipoRet.OK, " Ingreso Sastifactorio   ");
       p.ver(s.registrarChofer("AMBMVD12222","JORGE","46559714"), Isistema.TipoRet.ERROR, " La ambulancia ID no existe    ");
       
       
       p.ver(s.eliminarChofer("AMBMVD1","46559714"), Isistema.TipoRet.OK, " Se elimina Al chofer   ");
       p.ver(s.eliminarChofer("AMBMVD111111","46559714"), Isistema.TipoRet.ERROR, " LA ambulancia no existe   ");
       
        ////////////////////
       p.ver(s.informeChoferes("AMBMVD1"), Isistema.TipoRet.OK, "   ");
       
       
       
       
       
       
       
      
      
      
      
        
     
     
     
    
    
    
    
    
    
    
    
    
////    p.ver(s.agregarCiudad("Montevideo"), Isistema.TipoRet.OK, "Se agrega Montevideo al sistema ");
////    p.ver(s.agregarCiudad("Montevideo"), Isistema.TipoRet.ERROR, "Se intenta agregar Montevideo que ya existe ");
////    Ciudades hasta el limite
////    p.ver(s.agregarCiudad("San Jose"), Isistema.TipoRet.OK, "Se agrega San Jose al sistema ");
////    p.ver(s.agregarCiudad("Canelones"), Isistema.TipoRet.OK, "Se agrega Canelones al sistema ");
////    p.ver(s.agregarCiudad("Tacuarembo"), Isistema.TipoRet.OK, "Se agrega Tacuarembo al sistema ");
////    p.ver(s.agregarCiudad("Salto"), Isistema.TipoRet.OK, "Se agrega Salto al sistema ");
////    Ciudades fuera del limite
////    p.ver(s.agregarCiudad("Artigas"), Isistema.TipoRet.ERROR, "Se intenta agregar Artigas  y se sobrepasa del limite  ");
////    p.imprimirResultadosPrueba();
    
    
    }
    
}
