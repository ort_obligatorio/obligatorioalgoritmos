
package obligatorioalgoritmos1;


public class NodoAmbulancia {
    
    private String ambulanciaID;
    public NodoAmbulancia siguiente;
    private int ciudadID;
    private ListaChoferes choferes;
    private String ciudadUbicacion;

    public NodoAmbulancia(String ambulanciaID) {
        this.ambulanciaID = ambulanciaID;
        this.ciudadID = -1;
        this.choferes = null;
        this.ciudadUbicacion = null;
        this.estado = false;
        this.siguiente = null;
    }

    public String getAmbulanciaID() {
        return ambulanciaID;
    }

    public NodoAmbulancia getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoAmbulancia siguiente) {
        this.siguiente = siguiente;
    }

    public void setAmbulanciaID(String ambulanciaID) {
        this.ambulanciaID = ambulanciaID;
    }

    public int getCiudadID() {
        return ciudadID;
    }

    public void setCiudadID(int ciudadID) {
        this.ciudadID = ciudadID;
    }

    public ListaChoferes getChoferes() {
        return choferes;
    }

    public void setChoferes(ListaChoferes choferes) {
        this.choferes = choferes;
    }

    public String getCiudadUbicacion() {
        return ciudadUbicacion;
    }

    public void setCiudadUbicacion(String ciudadUbicacion) {
        this.ciudadUbicacion = ciudadUbicacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    private boolean estado;
  
    public void habilitar(){
    this.estado=true;
    }
    public void deshabilitar(){
    this.estado=false;
    }    
 
    
}
