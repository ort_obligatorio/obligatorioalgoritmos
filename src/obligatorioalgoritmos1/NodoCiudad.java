
package obligatorioalgoritmos1;


public class NodoCiudad {
 
    private String ciudadNombre;
    private ListaAmbulancias ambulancias;
    private  int ciudadID;
    private static int contador;            
    private NodoCiudad izq;
    private NodoCiudad der;
    
    public NodoCiudad(String ciudadNombre) {
        this.ciudadNombre = ciudadNombre;
        this.ambulancias = null;
        this.contador++;
        this.ciudadID = contador;
        this.izq = null;
        this.der = null;
    }

    public String getCiudadNombre() {
        return ciudadNombre;
    }

    public void setCiudadNombre(String ciudadNombre) {
        this.ciudadNombre = ciudadNombre;
    }

    public ListaAmbulancias getAmbulancias() {
        return ambulancias;
    }

    public void setAmbulancias(ListaAmbulancias ambulancias) {
        this.ambulancias = ambulancias;
    }

    public int getCiudadID() {
        return ciudadID;
    }

    public void setCiudadID(int ciudadID) {
        this.ciudadID = ciudadID;
    }

    public NodoCiudad getIzq() {
        return izq;
    }

    public void setIzq(NodoCiudad izq) {
        this.izq = izq;
    }

    public NodoCiudad getDer() {
        return der;
    }

    public void setDer(NodoCiudad der) {
        this.der = der;
    }
    
    
}
