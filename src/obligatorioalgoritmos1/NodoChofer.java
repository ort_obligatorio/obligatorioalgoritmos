
package obligatorioalgoritmos1;


public class NodoChofer {
    private String nombre;
    private String cedula;
    private String ambulanciaID;

    public NodoChofer(String nombre, String cedula) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.ambulanciaID = null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getAmbulanciaID() {
        return ambulanciaID;
    }

    public void setAmbulanciaID(String ambulanciaID) {
        this.ambulanciaID = ambulanciaID;
    }
    
}
