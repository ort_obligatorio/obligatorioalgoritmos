
package obligatorioalgoritmos1;


public interface IMatrizRecorridos {
   public void agregar(Object o);
    public void eliminar(Object o);
    public boolean estaVacia();
    public boolean estaLlena();
    public boolean existe(Object o);
    public int cantidadNodos();
     
}
