
package obligatorioalgoritmos1;


public interface IABBCiudades {
    public void agregarCiudad(String nombreciudad); 
    public void eliminar(Object o);
    public void vaciar();
    public boolean estaVacia();
    public boolean estaLlena();
    public boolean existe(Object o);
    public int cantidadNodos();
    public NodoCiudad buscarciudad(String ciudadNombre);
    public NodoCiudad buscarCiudadPorID(int ciudadID);
    
    

    
}
