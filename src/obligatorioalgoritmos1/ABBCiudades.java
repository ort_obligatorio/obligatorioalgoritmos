
package obligatorioalgoritmos1;


public class ABBCiudades  implements IABBCiudades{
  public NodoCiudad raiz;
  public int maxCantidad;
  public ListaAmbulancias ambulancias;
  public int contadorCiudad;

    public ABBCiudades(int maxCantidad) {
        this.raiz = null;
        this.maxCantidad = maxCantidad;
        this.ambulancias = null;
        this.contadorCiudad = 0;
    }

     public ABBCiudades() {
        this.raiz = null;
        this.maxCantidad = 0;
        this.ambulancias = null;
        this.contadorCiudad = 0;
    }

    public NodoCiudad getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoCiudad raiz) {
        this.raiz = raiz;
    }

    public int getMaxCantidad() {
        return maxCantidad;
    }

    public void setMaxCantidad(int maxCantidad) {
        this.maxCantidad = maxCantidad;
    }

    public ListaAmbulancias getAmbulancias() {
        return ambulancias;
    }

    public void setAmbulancias(ListaAmbulancias ambulancias) {
        this.ambulancias = ambulancias;
    }

    public int getContadorCiudad() {
        return contadorCiudad;
    }

    public void setContadorCiudad(int contadorCiudad) {
        this.contadorCiudad = contadorCiudad;
    }
    
    
     @Override
    public void agregarCiudad(String nombreciudad) {
    
        
	if(this.estaVacia()){
			NodoCiudad nuevo= new NodoCiudad(nombreciudad);
			this.raiz = nuevo;
                        
		}else{
                        NodoCiudad nuevo= new NodoCiudad(nombreciudad);
			ABBCiudades aux = new ABBCiudades();
			if (this.raiz.getCiudadNombre().compareTo(nuevo.getCiudadNombre())>0){
				aux.raiz = this.raiz.getIzq();
				aux.agregarCiudad(nuevo.getCiudadNombre());
				this.raiz.setIzq(aux.raiz);
			}
			if (this.raiz.getCiudadNombre().compareTo(nuevo.getCiudadNombre())<0){
				aux.raiz = this.raiz.getDer();
				aux.agregarCiudad(nuevo.getCiudadNombre());
				this.raiz.setDer(aux.raiz);
			}
			
		}
    }
    
    

    @Override
    public void eliminar(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean estaVacia() {
        return (this.raiz == null);
    }

    @Override
    public boolean estaLlena() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean existe(Object o) {
        if(!this.estaVacia()){
                        NodoCiudad nuevo=(NodoCiudad)o;
			ABBCiudades aux = new ABBCiudades();
			if (this.raiz.getCiudadNombre().equals(nuevo.getCiudadNombre())){
				return true;
			}
                        if (nuevo.getCiudadNombre().compareTo(this.raiz.getCiudadNombre())<0){
				aux.raiz = this.raiz.getIzq();
			}
			if (nuevo.getCiudadNombre().compareTo(this.raiz.getCiudadNombre())>0){
				aux.raiz = this.raiz.getDer();
			}
			return aux.existe(o);
		}
		return false;
    }

    @Override
    public int cantidadNodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    // PREGUNTAR!! BUSCO POR CIUDAD NOMBRE? Y CIUDAD ID?
    @Override
    public NodoCiudad buscarciudad(String ciudadNombre) {
      NodoCiudad nuevo=raiz;
            if(!this.estaVacia()){
                      
                        nuevo.setCiudadNombre(ciudadNombre);
			ABBCiudades aux = new ABBCiudades();
			if (this.raiz.getCiudadID()==(nuevo.getCiudadID())){
				return nuevo;
			}
                        if (nuevo.getCiudadID()<this.raiz.getCiudadID()){
				aux.raiz = this.raiz.getIzq();
			}
			if (nuevo.getCiudadID()>(this.raiz.getCiudadID())){
				aux.raiz = this.raiz.getDer();
			}
			return aux.buscarciudad(ciudadNombre);
		}
		return nuevo; 
        }
    
    
    public NodoCiudad buscarciudadxid(int ciudadID,NodoCiudad nodo ) {
      NodoCiudad nuevo = null;
            if(!this.estaVacia()){
                if(ciudadID==nodo.getCiudadID()){
                     return nodo;
                }
                 else{
                     if(nodo.getCiudadID()<ciudadID){
                         if(nodo.getIzq()!=null){
                         nuevo = buscarciudadxid(ciudadID,nodo.getIzq());
                         }
                         
                     }
                     else{
                         if(nodo.getDer()!=null){
                         nuevo = buscarciudadxid(ciudadID,nodo.getDer());
                         }
                     }
                 }
            }
       return nuevo;     
    }
    
  @Override
    public NodoCiudad buscarCiudadPorID(int ciudadID){
    
    NodoCiudad nuevo=raiz;
            if(!this.estaVacia()){
                      
                        nuevo.setCiudadID(ciudadID); //esto podria estar arriba
			ABBCiudades aux = new ABBCiudades();
			if (this.raiz.getCiudadID()==(nuevo.getCiudadID())){
				return nuevo;
			}
                        if (nuevo.getCiudadID()<this.raiz.getCiudadID()){
				aux.raiz = this.raiz.getIzq();
			}
			if (nuevo.getCiudadID()>(this.raiz.getCiudadID())){
				aux.raiz = this.raiz.getDer();
			}
			return aux.buscarCiudadPorID(ciudadID);
		}
		return nuevo; 
    }
    
    public void listoCiudadesEnOrden() {
        if (!this.estaVacia()) {
            ABBCiudades auxIzq = new ABBCiudades();
            auxIzq.raiz = this.raiz.getIzq();
            auxIzq.listoCiudadesEnOrden();

            System.out.print(this.raiz.getCiudadID()+", ");

            ABBCiudades auxDer = new ABBCiudades();
            auxDer.raiz = this.raiz.getDer();
            auxDer.listoCiudadesEnOrden();
        }
        
        
    }
    
    @Override
    public void vaciar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
   

   
    
}
    

