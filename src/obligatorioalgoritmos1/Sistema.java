
package obligatorioalgoritmos1;


public class Sistema implements Isistema {
 
    private ABBCiudades arbolCiudad;
    
    
 
    @Override
    public TipoRet crearSistemaDeEmergencias(int cantidadCiudades) {
       
        if(cantidadCiudades<0){
                return TipoRet.ERROR;

        }
          else
            this.arbolCiudad = new ABBCiudades(cantidadCiudades);
            return TipoRet.OK;
    }

    @Override
    public TipoRet destruirSistemaEmergencias() {
        this.arbolCiudad.vaciar();
        return TipoRet.OK;
    
    }
//////////////////
    @Override
    public TipoRet registrarAmbulancia(String ambulanciaId, int ciudadID) {
    
        
        NodoCiudad ciu =  arbolCiudad.buscarciudadxid(ciudadID,arbolCiudad.raiz);
     if(ciu==null){
            return TipoRet.ERROR;
     }
     if (ciu.getAmbulancias() == null) {
            ciu.setAmbulancias(new ListaAmbulancias());
            NodoAmbulancia amb = new NodoAmbulancia(ambulanciaId);
            ciu.getAmbulancias().agregar(amb);
           
            
            
            return TipoRet.OK;

        } else if (ciu.getAmbulancias().existe(ciu)) {
            
            return TipoRet.ERROR;

        } else {

            NodoAmbulancia amb = new NodoAmbulancia(ambulanciaId);
            ciu.getAmbulancias().agregar(amb);
            
           // arbolCiudad.AgregarHotelAux(hotel);
            return TipoRet.OK;

        }

     }

    @Override
    public TipoRet deshabilitarAmbulancia(String ambulanciaId) {
           NodoAmbulancia amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaId) ;
        if (amb == null) {
            return TipoRet.ERROR;
        }
          if(amb.isEstado()==false){
        return TipoRet.ERROR;
        }
        
         else {
            amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaId);
            amb.deshabilitar();
            return TipoRet.OK;
        }
    }

    @Override
    public TipoRet habilitarAmbulancia(String ambulanciaID) {
        NodoAmbulancia amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaID) ;
        if (amb == null) {
            return TipoRet.ERROR;
        } 
        if(amb.isEstado()==true){
        return TipoRet.ERROR;
        }
         else {
            amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaID);
            amb.habilitar();
            return TipoRet.OK;
        }
    }
    
    @Override
    public TipoRet eliminarAmbulancia(String ambulanciaId) {
        NodoAmbulancia amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaId) ;
        
        if(amb!=null){
            this.arbolCiudad.getAmbulancias().eliminar(amb);
            return TipoRet.OK;
        }
        else{
            return TipoRet.ERROR;
        }
        
        
        
        
    }
    
    @Override
    public TipoRet buscarAmbulancia(String ambulanciaID) {
       NodoAmbulancia amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaID);
        if (amb == null) {
            return TipoRet.ERROR;
        } 
         else {
            amb = this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaID);
            amb.deshabilitar();
            return TipoRet.OK;
        }  
    }

    @Override
    public TipoRet informeAmbulancia() {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet informeAmbulancia(int ciudadID) {
        NodoCiudad ciu = this.arbolCiudad.buscarCiudadPorID(ciudadID);
        ciu.getAmbulancias().amb.getAmbulanciaID();
        ciu.getAmbulancias().amb.isEstado();
        ciu.getAmbulancias().amb.getCiudadUbicacion();
        
        return TipoRet.OK;
    }

    @Override
    public TipoRet cambiarUbicacion(String ambulanciaID, int ciudadID) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet agregarCiudad(String ciudadNombre) {
    NodoCiudad nodo = new NodoCiudad(ciudadNombre);
        if (!arbolCiudad.existe(nodo)) { //Busca el si existe el nodo en el arbol
            
            if(arbolCiudad.maxCantidad>0){
            arbolCiudad.agregarCiudad(ciudadNombre); //Agrega el nodo al arbol
            return TipoRet.OK;
            
            }else{
            
            return TipoRet.ERROR;
            }
        }
        return TipoRet.ERROR;
    }

    @Override
    public TipoRet listarCiudades() {
        arbolCiudad.listoCiudadesEnOrden();
        return TipoRet.OK;
    }

    @Override
    public TipoRet agregarRuta(int ciudadOrigen, int ciudadDestino, int minutosViaje) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet modificarDemora(int ciudadOrigen, int ciudadDestino, int minutosViaje) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet ambulanciaMasCercana(int ciudadID) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet rutaMasRapida(int ciudadOrigen, int ciudadDestino) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet informeCiudades() {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet ciudadesEnRadio(int ciudadID, int duracionViaje) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet registrarChofer(String ambulanciaID, String nombre, String cedula) {
        NodoAmbulancia amb =  this.arbolCiudad.getAmbulancias().buscarAmbulancia(ambulanciaID);
         amb.getChoferes().AgregarChofer(nombre,cedula);
        
    }

    @Override
    public TipoRet eliminarChofer(String ambulanciaID, String cedula) {
        return TipoRet.NO_IMPLEMENTADA;
    }

    @Override
    public TipoRet informeChoferes(String ambulanciaID) {
        return TipoRet.NO_IMPLEMENTADA;
    }
    
}
